var express = require('express')
var path = require('path')
//routes
var studentRoutes = require('./api/routes/student')

var userRoutes = require('./api/routes/user')

var imageRoutes = require('./api/routes/image')
//end routes

//database
var dbConn = require('./db/connect')

dbConn.connect()
//

var app = express()

var bodyParser = require('body-parser')

app.use(bodyParser.urlencoded({
    extended: true
}))

app.use(bodyParser.json())

app.use('/uploads', express.static(path.join(__dirname, 'uploads')))

app.get('/upload', function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

app.use('/students', studentRoutes)
app.use('/users', userRoutes)
app.use('/', imageRoutes)

app.listen(1923, function(){
    console.log("Server has started on http://localhost:1923")
})