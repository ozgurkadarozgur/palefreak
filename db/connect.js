var mongoose = require('mongoose')

var mongoDB = 'mongodb://127.0.0.1/student-management'


var connect = () => {

    mongoose.connect(mongoDB, { useNewUrlParser: true })

    var db = mongoose.connection
    
    db.on('error', console.error.bind(console, 'MongoDB conection error:'))
    
}

module.exports = {
    connect
}