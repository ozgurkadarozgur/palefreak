
const mongoose = require("mongoose");

const User = require('../models/user')

const rt = require('../helper/responseTypes')

upload = async (req, res) => { 
    await User.findOne({username: req.user.username}, function(err, user){
        console.log('fromreq', req.user.username) 

        console.log('fromdb', user.username)
        if (err) console.log('err', err)
        if(user) {
            console.log(req.file)
            user.images.push({
                content_type: req.file.mimetype,
                path: req.file.path
            })
            user.save()
            res.status(200).json({
                status: 'success'                
            })
        } else {
            console.log('user not found')
            res.status(200).json({
                status: 'warning',
                type: rt.responseTypes.UserNotFound,
                message: 'Kullanıcı bulunamadı.'
            })
        }
    })
}

module.exports = {
    upload
}