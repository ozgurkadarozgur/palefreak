
const mongoose = require("mongoose");

const passwordHash = require('password-hash')

const User = require('../models/user')

//const FollowRequest = require('../models/followRequest')

const { validationResult } = require('express-validator');

const jwt = require('jsonwebtoken')

const numberGenerator = require('../helper/numberGenerator')

const rt = require('../helper/responseTypes')

index = async (req, res) => {
    await User.find().select('-password').exec(function(err, data){
        if (err) throw err;
        res.status(200).json(data)
    })
}

show = async (req, res) => {
    
    console.log(req.params.userid)
    
    await User.findById(req.params.userid, function(err, user){
        if (err) throw err
        if(user) {
            if(req.headers["authorization"]) {
                
            } else {
        
            }
            res.status(200).json(user)
        } else {
            res.status(200).json({
                status: 'warning',
                type: rt.responseTypes.UserNotFound,
                message: 'Kullanıcı bulunamadı.'
            })
        }
    })
}

profile = async (req, res) => {
    res.status(200).json({
        status: 'success',
        message: req.user
    })
}

store = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    try {
         await User.findOne({username: req.body.username}, function(err, result){
            if (err) console.log(err)
            if(result) {
                res.status(200).json({
                    status: 'warning',
                    type: rt.responseTypes.UserNameExists,
                    message: 'Kullanıcı adı başkası tarafından kullanılıyor.'
                })
            } else {
                const user = new User({
                    _id: new mongoose.Types.ObjectId,
                    username: req.body.username,
                    password: passwordHash.generate(req.body.password),
                    city: req.body.city,
                    gender: req.body.gender,
                    phone: req.body.phone,
                    phoneConfirmed: false,
                    phoneKey: numberGenerator.generateNumber(),
                    preferences: JSON.parse(req.body.preferences)
                })
            
                user.save()
            
                res.status(200).json({
                    status: 'success',
                    data: {
                        username: user.username,                
                        city: user.city,
                        gender: user.gender,
                        phone: user.phone,
                        preferences: user.preferences
                    }
                })
            }
            
        })
    }catch(e) {
        console.log(e)
    }
   
}

update = async (req, res) => {
    await User.findOne({username: req.user.username}, function(err, user) {
        if (err) throw err
        if (user) {
            user.update(req.body)
            res.status(200).json(user)
        } else {
            res.status(200).json({
                status: 'warning',
                type: rt.responseTypes.UserNotFound,
                message: 'Kullanıcı bulunamadı.'
            })
        }
    })
}

confirmPhone = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    try {
        await User.findOne({username: req.body.username}, function(err, user) {
            if (err) console.log(err)
            if(user) {
                if (user.phoneKey == req.body.phoneKey) {
                    user.phoneConfirmed = true
                    user.save()
                    return res.status(200).json({
                        status: 'success'
                    })
                }else {
                    return res.status(200).json({
                        status: 'warning',
                        type: rt.responseTypes.InvalidPhoneCode,
                        message: 'Hatalı kod girildi.'
                    })
                }
            } else {
                res.status(200).json({
                    status: 'warning',
                    type: rt.responseTypes.UserNotFound,
                    message: 'Kullanıcı bulunamadı.'
                })
            }
        })
    } catch(e) {
        console.log(e)
    }
}

login = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    try{
        await User.findOne({username: req.body.username}, function(err, user){
            if(user) {
                var pw_match = passwordHash.verify(req.body.password, user.password)
                if(pw_match) {
                    console.log('pw matched')                    
                    if(user.phoneConfirmed) {                
                    jwt.sign({user}, 'secretkey', (err, token) => {
                        if (err) console.log(err)
                        
                        res.status(200).json({
                                token
                            })
                        })
                    } else {
                        res.status(200).json({
                            status: 'warning',
                            type: rt.responseTypes.PhoneNotConfirmed,
                            message: 'Telefon doğrulaması yapılmadı'
                        })
                    }   
                } else {
                        res.status(200).json({
                            status: 'warning',
                            type: rt.responseTypes.UserNotFound,
                            message: 'Kullanıcı bulunamadı.'
                        })
                    }                  
            } else {
                res.status(200).json({
                    status: 'warning',
                    type: rt.responseTypes.UserNotFound,
                    message: 'Kullanıcı bulunamadı.'
                })
            }
        })
    } catch(e) {
        console.log(e)
    }
}

findByPreferences = async function(req, res) {
    await User.find({
        preferences: {$in: [1]}
    }).exec((err, data) => {
        if(err) throw err
        res.json(data)
    })
}

follow = async function(req, res) {

    await User.findById(req.user._id, function(err, user) {
        if (err) throw err
        if (user) {                               
            //user.followList.push(req.body.followed);
            //user.save() 
            followRequest(req.body.followed, user._id)
        } else {
            res.status(200).json({
                status: 'warning',
                type: rt.responseTypes.UserNotFound,
                message: 'Kullanıcı bulunamadı.'
            })
        }
    })

    res.status(200).json({
        status: 'success',
        data: req.user       
    })

}

followRequestList = async function(req, res) {
    
    await User.findById(req.user._id, function(err, user) {
        if (err) throw err
        if (user) {   
            console.log('frl', user) 
            followerIds = user.followRequestList.map(item => item._id);
            User.find({
                _id: {$in: followerIds}
            }).exec((err, data) => {
                if(err) throw err
                res.json(data)
            })            
            //res.json(user.followList)           
        } else {
            res.status(200).json({
                status: 'warning',
                type: rt.responseTypes.UserNotFound,
                message: 'Kullanıcı bulunamadı.'
            })
        }
    })
}

acceptFollow = async function(req, res) {

    await User.findById(req.user._id, function(err, user) {
        if (err) throw err
        if (user) {   
            user.followList.push(req.body.userId)      
            let removeIndex = user.followRequestList.map((item) => item).indexOf(req.body.userId);
            user.followRequestList.splice(removeIndex, 1);      
            user.save()
            User.findById(req.body.userId, function(err, userf) {
                if (err) throw err
                if (userf) {
                    userf.followList.push(user._id)
                    userf.save()
                }
            })
            res.status(200).json({
                status: 'success'
            })
        } else {
            res.status(200).json({
                status: 'warning',
                type: rt.responseTypes.UserNotFound,
                message: 'Kullanıcı bulunamadı.'
            })
        }
    })

}

rejectFollow = async function(req, res) {
    await User.findById(req.user._id, function(err, user) {
        if (err) throw err
        if (user) {  
                              
            let removeIndex = user.followRequestList.map((item) => item).indexOf(req.body.userId);
            user.followRequestList.splice(removeIndex, 1);      
            user.save()
            
            res.status(200).json({
                status: 'success'
            })
        } else {
            res.status(200).json({
                status: 'warning',
                type: rt.responseTypes.UserNotFound,
                message: 'Kullanıcı bulunamadı.'
            })
        }
    })
}

followRequest = function (to, from) {
    User.findById(to, function(err, user) {
        if (err) throw err
        if (user) {                               
            user.followRequestList.push(from._id);
            user.save()            
        }
    })
}

followList = async function(req, res) {
    await User.findById(req.user._id, function(err, user) {
        if (err) throw err
        if (user) {    
            User.find({
                _id: {$in: user.followList}
            }).exec((err, data) => {
                if(err) throw err
                res.json(data)
            })            
            //res.json(user.followList)           
        } else {
            res.status(200).json({
                status: 'warning',
                type: rt.responseTypes.UserNotFound,
                message: 'Kullanıcı bulunamadı.'
            })
        }
    })
    /*
    await User.find({
        _id: {$in: req.user.followList}
    }).exec((err, data) => {
        if(err) throw err
        res.json(data)
    })
    */
}

module.exports = {
    index,
    store,
    update,
    login,
    show,
    profile,
    findByPreferences,
    confirmPhone,
    follow,
    followList,
    followRequestList,
    acceptFollow,
    rejectFollow
}