
const mongoose = require("mongoose");

const Student = require('../models/student')

const { validationResult } = require('express-validator');

index = async (req, res) => {
    await Student.find().exec().then(function(err, data){
        if (err) throw err;
        res.status(200).json(data)
    }).catch(err => {
        console.log(err)
        res.status(500).json({
            error: err
        })
    })
}

store = async (req, res) => {

    //const errors = validationResult(req)
    //console.log(errors)
    
    const student = new Student({
        _id: new mongoose.Types.ObjectId,
        name: req.body.name
    })

   
    await student.save()

    res.send(req.body)

}

module.exports = {
    index,
    store
}