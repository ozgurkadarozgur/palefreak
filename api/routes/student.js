const express = require('express')

const router = express.Router()

const studentController = require('../controllers/student')

const { check } = require('express-validator');


router.get('/', studentController.index)

router.post('/', studentController.store)

module.exports = router