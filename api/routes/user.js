const express = require('express')

const router = express.Router()

const userController = require('../controllers/user')

const jwtHelper = require('../helper/jwt')

const { check } = require('express-validator');

router.get('/' ,userController.index)

router.post('/',[
    check('username').exists().withMessage('Kullanıcı adı boş bırakılamaz.'),
    check('password').exists().withMessage('Şifre boş bırakılamaz.'),
    check('preferences').exists().withMessage('Tercihler boş bırakılamaz'),
    check('city').exists().withMessage('Şehir boş bırakılamaz'),
    check('gender').exists().withMessage('Cinsiyet boş bırakılamaz'),
    check('phone').exists().withMessage('Telefon boş bırakılamaz')
], userController.store)

router.get('/show/:userid', userController.show)

router.post('/update', jwtHelper.verifyToken, userController.update)

router.get('/profile', jwtHelper.verifyToken, userController.profile)

router.post('/login', [
    check('username').exists().withMessage('Kullanıcı adı boş bırakılamaz.'),
    check('password').exists().withMessage('Şifre boş bırakılamaz.'),
] ,userController.login)

router.get('/find-by-preferences', userController.findByPreferences)

router.post('/confirm-phone', [
    check('username').exists().withMessage('Kullanıcı adı boş bırakılamaz.'),
    check('phoneKey').exists().withMessage('Telefon kodu boş bırakılamaz.'),
],userController.confirmPhone)

router.post('/follow', jwtHelper.verifyToken, userController.follow)

router.get('/followlist', jwtHelper.verifyToken, userController.followList)

router.get('/follow-request-list', jwtHelper.verifyToken, userController.followRequestList)

router.post('/accept-follow', jwtHelper.verifyToken, userController.acceptFollow)

router.post('/reject-follow', jwtHelper.verifyToken, userController.rejectFollow)

module.exports = router