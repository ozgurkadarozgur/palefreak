const express = require('express')

const router = express.Router()

const imageController = require('../controllers/image')

const multer  = require('multer')

var upload = multer({ dest: 'uploads/' })

const jwtHelper = require('../helper/jwt')

router.post('/upload-photo', jwtHelper.verifyToken, upload.single('avatar'), imageController.upload)

module.exports = router