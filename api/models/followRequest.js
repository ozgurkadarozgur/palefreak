const mongoose = require('mongoose')

const followRequestSchema = mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    follower: String,
    followed: String,
    isConfirmed: Boolean
},{ versionKey: false })

module.exports = mongoose.model('followRequests', followRequestSchema)