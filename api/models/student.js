const mongoose = require('mongoose')

const studentSchema = mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    name: String
},{ versionKey: false })

module.exports = mongoose.model('students', studentSchema)