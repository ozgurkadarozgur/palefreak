const mongoose = require('mongoose')

const userSchema = mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    username: String,
    firstName: String,
    lastName: String,
    password: String,
    gender: String,
    email: String,
    aboutme: String,
    city: String,
    birthday: mongoose.Schema.Types.Date,
    phone: String,
    phoneConfirmed: mongoose.Schema.Types.Boolean,
    phoneKey: String,
    preferences: [String],
    images: [{}],
    followList: [mongoose.Schema.Types.ObjectId],
    followRequestList: [mongoose.Schema.Types.ObjectId]    
},{ versionKey: false })

module.exports = mongoose.model('users', userSchema)