const jwt = require('jsonwebtoken')

//Verify Token
verifyToken = function verifyToken(req, res, next) {
    //Get auth header value
    const bearerHeader = req.headers['authorization']    
    console.log(bearerHeader)
    //Check if bearer is undefined
    if(typeof bearerHeader !== 'undefined') {
        //Split at the space
        const bearer = bearerHeader.split(' ')
        const bearerToken = bearer[1]
        jwt.verify(bearerToken, 'secretkey', (err, authData) => {
            if(err) {
                res.status(403).json({
                    message: 'Unauthorized'
                })
            }else {
                console.log(authData)
                req.token = bearerToken
                req.user = authData.user
                next()
            }
        })
    } else {
        // Forbidden
        res.status(403).json({
            message: 'Unauthorized'
        })
    }

}

module.exports = {
    verifyToken
}