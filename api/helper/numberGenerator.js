generateNumber = function() {
    
    var high = 99999
    var low = 10000
    return Math.floor(Math.random() * (high - low) + low)
      
}

module.exports = {
    generateNumber
}