
var responseTypes = {
    UserNotFound: 'userNotFound',
    PhoneNotConfirmed: 'phoneNotConfirmed',
    InvalidPhoneCode: 'invalidPhoneCode',
    UserNameExists: 'userNameExists',
    UserFollowing: 'userFollowing',
    FollowRequestHasSent: 'followRequestHasSent'
}

module.exports = {
    responseTypes
}